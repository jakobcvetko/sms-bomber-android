package com.pimp.smsbomber.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;
import android.util.Log;

import com.pimp.smsbomber.models.Message;
import com.pimp.smsbomber.network.AsyncRequest;

import java.io.Console;
import java.util.Arrays;
import java.util.Date;

public class SmsListener extends BroadcastReceiver {

    private final String SMSIntent="android.provider.Telephony.SMS_RECEIVED";
	
	@Override
	public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(SMSIntent)){
            if(intent.getExtras()!=null){
                Log.e("HELLO","message recieved");
                SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent); //ignore
                for (int i=0; i< messages.length; i++){
                    SmsMessage message = messages[i];

                    Message sms = new Message();
                    sms.setMessage(message.getDisplayMessageBody());
                    sms.setMessage_id(message.getIndexOnIcc());
                    sms.setAndroid_id(message.getDisplayMessageBody());
                    sms.setType(message.getProtocolIdentifier());
                    sms.setAddress(message.getOriginatingAddress());
                    Date sent_at = new Date();
                    sms.setSent_at(sent_at);

                    AsyncRequest req = new AsyncRequest();
                    req.addMessageToQuery(sms);
                    req.execute();
                }
            }
        }
	}    
}

