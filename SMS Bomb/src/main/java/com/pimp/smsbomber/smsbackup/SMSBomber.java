package com.pimp.smsbomber.smsbackup;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pimp.smsbomber.R;
import com.pimp.smsbomber.alarm.SmsListener;
import com.pimp.smsbomber.models.Message;
import com.pimp.smsbomber.network.AsyncRequest;

public class SMSBomber extends Activity {

    private final int CONTACT_PICK = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IntentFilter mIntentFilter = new IntentFilter();
        mIntentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(new SmsListener(), mIntentFilter);
        /*
        Intent intent = new Intent(getBaseContext(), SmsListener.class);
        intent.setAction("si.jboss.smsbackup.ACTION");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),
                0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        AlarmManager alarm = (AlarmManager) getBaseContext().getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pendingIntent);
        alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        */
        AsyncRequest req = new AsyncRequest();
        req.setContext(this);
        req.addMessageToQuery(null);
        req.execute();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.smsbomber, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void pickContact(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, 1);
    }
    public void plus(View view) {
        EditText numberBox = (EditText) findViewById(R.id.number);
        if(numberBox.getText()!=null){
            if(numberBox.getText().toString().length()>0){
                int i = Integer.parseInt(numberBox.getText().toString());
                numberBox.setText(String.valueOf(++i));
            }
            else {
                numberBox.setText("1");
            }
        }
    }
    public void minus(View view) {
        EditText numberBox = (EditText) findViewById(R.id.number);
        if(numberBox.getText()!=null){
            if(numberBox.getText().toString().length()>0){
                int i = Integer.parseInt(numberBox.getText().toString());
                if(i<=1){
                    numberBox.setText("1");
                }
                else{
                    numberBox.setText(String.valueOf(--i));
                }
            }
            else {
                numberBox.setText("1");
            }
        }
    }

    public void sendSMS(View view){
        EditText messageBox = (EditText) findViewById(R.id.message);
        EditText numberBox = (EditText) findViewById(R.id.number);
        EditText contactBox = (EditText) findViewById(R.id.contact);
        Sender sender = new Sender();
        if(numberBox.getText().toString().length()>0 && contactBox.getText().toString().length()>0){
            Message message = new Message();
            message.setAddress(contactBox.getText().toString());
            message.setMessage(messageBox.getText().toString());
            sender.sendSMS(this,message,Integer.parseInt(numberBox.getText().toString()));
            messageBox.setText("");
        }
        else if (contactBox.getText().toString().length()<1){
            new AlertDialog.Builder(this)
                    .setTitle("Not so fast!")
                    .setMessage("Please enter or select contact number...")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }
                    )
                    .show();
        }
        else if(numberBox.getText().toString().length()<1){
            numberBox.setText("1");
            //sender.sendSMS(this,message,Integer.parseInt(numberBox.getText().toString()));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case (CONTACT_PICK):
                if (resultCode == Activity.RESULT_OK)
                {
                    Uri contactData = data.getData();
                    Cursor c = managedQuery(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String id =
                                c.getString(c.getColumnIndexOrThrow(ContactsContract.Contacts._ID));

                        String hasPhone =
                                c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));

                        if (hasPhone.equalsIgnoreCase("1")) {
                            Cursor phones = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                                    null, null);
                            phones.moveToFirst();
                            EditText contact = (EditText) findViewById(R.id.contact);
                            contact.setText(phones.getString(phones.getColumnIndex("data1")));
                            Button send = (Button) findViewById(R.id.sendsms);
                            send.setEnabled(true);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Izberi kontak s številko!", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                break;
        }
    }
}
