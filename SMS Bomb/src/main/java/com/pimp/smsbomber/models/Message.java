package com.pimp.smsbomber.models;

import java.util.Date;

import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

public class Message {

	private long message_id;
	private String android_id;
	private int type;
	private String address;
	private String message;
	private Date sent_at;

	// Constants
	private static final int RECEIVED_TYPE = 1;
	private static final int SENT_TYPE = 2;

	public boolean is_type_received() {
		return type == RECEIVED_TYPE;
	}

	public boolean is_type_sent() {
		return type == SENT_TYPE;
	}

	public void setAddress(String address) {
		try {
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			PhoneNumber number = phoneUtil.parse(address, "SI");
			address = phoneUtil.format(number, PhoneNumberFormat.E164);
		} catch (NumberParseException e) {
			Log.e("Error", "Cant parse phonenumber " + address);
		}
		this.address = address;
	}

	public String getAddress() {
		return address;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Date getSent_at() {
		return sent_at;
	}
	public void setSent_at(Date sent_at) {
		this.sent_at = sent_at;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getAndroid_id() {
		return android_id;
	}
	public void setAndroid_id(String android_id) {
		this.android_id = android_id;
	}
	public long getMessage_id() {
		return message_id;
	}
	public void setMessage_id(long message_id) {
		this.message_id = message_id;
	}

    @Override
    public String toString() {
        return "ADDRESS: "+getAddress()+" MESS: "+getMessage();
    }
}
